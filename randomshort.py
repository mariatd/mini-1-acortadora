import webapp
import shelve
import string
import random
from urllib import parse

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Url a introducir: </label>
        <input type="text" name="url" required>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Content for {resource}:</p> 
    <div>
    <div>
        {form}
    <div>
    <div>
        <p>Urls acortadas: {dic}<p>
    <div>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Recurso no encontrado: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
    <div>
        <p>Urls acortadas: {dic}<p>
    <div>
  </body>
</html>
"""

PAGE_POST = """
<!DOCTYPE html>
<html lang="en">
  <body>
  <div>
    <p>Url acortada: {body}.</p>
  <div>
  <div>
      {form}
  </div>
  <div>
        <p>Urls acortadas: {dic}<p>
  <div>
  </body>
</html>
"""

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Metodo no permitido: {method}.</p>
  </body>
</html>
"""

CODE_REDIRECT = "301 Moved Permanently\r\n" \
                + "Location: {location}\r\n\r\n"


class RandomShortApp(webapp.WebApp):

    def __init__(self, hostname, port):
        super().__init__(hostname, port)
        self.contents = shelve.open('contents')

    def parse(self, request):
        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            data['body'] = None
        else:
            data['body'] = request[body_start + 4:]

        parts = request.split(' ', 2)
        if len(parts) > 1:
            data['method'] = parts[0]
            data['resource'] = parts[1]
        else:
            data['method'] = None
            data['resource'] = None

        return data

    def process(self, data):
        if data['method'] == 'GET':
            code, page = self.get(data['resource'])
        elif data['method'] == 'POST':
            code, page = self.post(data['resource'], data['body'])
        else:
            code, page = "405 Method not allowed", \
                PAGE_NOT_ALLOWED.format(method=data['method'])
        return code, page

    def get(self, resource):
        if resource == '/':
            print(self.returndic(self.contents))
            page = PAGE.format(resource=resource, form=FORM, dic=self.returndic(self.contents))
            code = "200 OK"

        elif resource in self.contents:
            location = self.contents[resource]
            code = CODE_REDIRECT.format(location=location)
            page = ""

        elif resource == '/favicon.ico':
            code = "204 No Content"
            page = ""

        else:
            page = PAGE_NOT_FOUND.format(resource=resource, form=FORM, dic=self.returndic(self.contents))
            code = "404 Resource Not Found"

        return code, page

    def post(self, resource, body):
        fields = parse.parse_qs(body)
        if resource == '/':
            if 'url' in fields:
                url = fields['url'][0]
                if not (url.startswith("http://") or url.startswith("https://")):
                    url = "http://" + url

                if url not in self.contents.values():
                    random_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
                    short_url = '/' + random_key
                    self.contents[short_url] = url
                    page = PAGE_POST.format(body=short_url, form=FORM, dic=self.returndic(self.contents))
                    code = "200 OK"
                else:
                    key = list(self.contents.keys())[list(self.contents.values()).index(url)]
                    page = PAGE_POST.format(body=key, form=FORM, dic=self.returndic(self.contents))
                    code = "200 OK"
            else:
                page = PAGE_UNPROCESABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            page = PAGE_UNPROCESABLE.format(body=body)
            code = "422 Unprocessable Entity"

        return code, page

    def returndic(self, dic):
        text = "<br>"
        for element in dic:
            text = text + "Url original: " + dic[element] + "Url acortada: " + element + "<br>"
        return text


if __name__ == "__main__":
    try:
        webApp = RandomShortApp("localhost", 1234)
        webApp.accept_connections()
    except KeyboardInterrupt:
        print("Conexion finalizada")
