# webapp.py

import socket

class WebApp:

    def parse(self, request):
        print("Parse: No se está analizando nada")
        return None

    def process(self, parsedRequest):
        print("Process: Retornando 200 OK")
        return "200 OK", "<html><body><h1>¡Funciona!</h1></body></html>"

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port
        self.setup_socket()

    def setup_socket(self):
        self.mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.mySocket.bind((self.hostname, self.port))
        self.mySocket.listen(5)

    def accept_connections(self):
        try:
            while True:
                print("En espera de conexiones entrantes...")
                (recvSocket, address) = self.mySocket.accept()
                print("Solicitud HTTP recibida:")
                self.handle_request(recvSocket)
        except KeyboardInterrupt:
            print("Conexión finalizada")
        finally:
            self.mySocket.close()

    def handle_request(self, recvSocket):
        request = recvSocket.recv(2048)
        print(request)
        parsedRequest = self.parse(request.decode('utf8'))
        (returnCode, htmlAnswer) = self.process(parsedRequest)
        print("Respondiendo...")
        response = f"HTTP/1.1 {returnCode}\r\nContent-Length: {len(htmlAnswer)}\r\n\r\n{htmlAnswer}\r\n"
        recvSocket.send(response.encode('utf8'))
        recvSocket.close()



if __name__ == "__main__":
    testWebApp = WebApp("localhost", 1234)
